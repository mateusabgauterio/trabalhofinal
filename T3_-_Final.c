#include <stdlib.h>
#include <mpi.h>
#include <stdio.h>
#define ARRAY_SIZE 100

//using namespace std;

void BubbleSort(int* vetor, int n)
{
    int c=0, d, troca, trocou =1;

    while (c < (n-1) & trocou ){
        trocou = 0;

        for (d = 0 ; d < n - c - 1; d++)
            if (vetor[d] > vetor[d+1]){
                troca      = vetor[d];
                vetor[d]   = vetor[d+1];
                vetor[d+1] = troca;
                trocou = 1;
            }
        c++;
    }
}


int main(int argc, char** argv) {
    
    //Initialização
    double t1, t2, i, j;
    int intRank, proc_n;
    int fimPrograma;


    MPI_Status status;
    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&intRank);
    MPI_Comm_size(MPI_COMM_WORLD,&proc_n);
    t1 = MPI_Wtime();

    //FAZER UM VETOR NOVO DE ESTADOS PARA CONTROLE DE PROCESSOS
    int estadoOrdenado, estadoAcum, maiorAnt, tamVetor=ARRAY_SIZE/proc_n;
    int vetor[tamVetor], vetAux[tamVetor/2], vetOrdena[tamVetor], vetDesord[proc_n], vetDesordAux[proc_n];
    
    // gero parte local do vetor (1/np avos do vetor global)
    //Inicializa vetor com 1.000.000 de números em ordem decrescente
    printf("Inicializando vetores desordenado.");
    printf("...");
    for(int i = 0; i<tamVetor; i++){
        vetor[i] = ARRAY_SIZE - (tamVetor*intRank + i);
    } //1.000.000 - ((1.000.000/16) * (0->proc_n) + (0->1.000.000/16))

    //Inicializa o vetor para controlar os processos que estão ou não ok
    for(int i = 0; i<proc_n; i++){ 
        vetDesord[i]=0;
    }
    
    fimPrograma = 0; //Não é o fim

    //printf("Nois 0");

    while (fimPrograma==0)
    {  
        printf("%d-----\n", intRank);
        for(int i = 0; i<tamVetor; i++){
            printf("%d, ", vetor[i]);
        }
        printf('\n');

        // ordeno vetor local
        //printf("Nois 1");
        BubbleSort (vetor, tamVetor);

        // verifico condição de parada
        if (intRank < proc_n-1) {// se não for np-1, mando o meu maior elemento para a direita
            MPI_Send(&vetor[tamVetor-1], 1, MPI_INT, intRank +1, 0, MPI_COMM_WORLD);

        } else if(intRank > 0) {// se não for 0, recebo o maior elemento da esquerda
            MPI_Recv(&maiorAnt, 1, MPI_INT, 0, intRank-1, 0, &status);

        } else{ // comparo se o meu menor elemento é maior do que o maior elemento recebido (se sim, estou ordenado em relação ao meu vizinho)
            if (maiorAnt>vetor[0])
                vetDesord[intRank] = 1; //Não está ordenado
        }
        
        // compartilho o meu estado com todos os processos, fazendo um BCAST com cada processo como origem (NP vezes)
        for (int i=0; i<proc_n-1; i++){
            MPI_Bcast(&vetDesord, proc_n, MPI_INT, i, MPI_COMM_WORLD);
            MPI_Bcast(&vetDesordAux, proc_n, MPI_INT, MPI_ANY_TAG, MPI_COMM_WORLD);
            for(int i = 0; i<proc_n; i++){  //Vê quais estavam desordenados
                if(vetDesord[i]<vetDesordAux[i]){
                    vetDesord[i]=vetDesordAux[i];
                    estadoOrdenado=1;
                }
            }
        }
        
        // se todos estiverem ordenados com seus vizinhos, a ordenação do vetor global está pronta ( pronto = TRUE, break)
        if(estadoOrdenado==0){
            fimPrograma = 1;
            break;
        } else{
            fimPrograma = 0;     

            // troco valores para convergir
            if(intRank > 0){// se não for o 0, mando os menores valores do meu vetor para a esquerda   
                printf("Nois 0");
                for(int i=0; i<tamVetor/2; i++){
                    vetAux[i] = vetor[i];
                }
                MPI_Send(&vetAux, tamVetor/2, MPI_INT, intRank-1, 0, MPI_COMM_WORLD); 

            } else if (intRank < proc_n-1){ // se não for np-1, recebo os menores valores da direita
                printf("Nois 1");
                MPI_Recv(&vetAux, tamVetor/2, MPI_INT, 0, intRank+1, 0, &status);
                // ordeno estes valores com a parte mais alta do meu vetor local
                int j=0;
                for(int i=tamVetor/2; i<tamVetor; i++){
                    vetOrdena[j] = vetor[i];
                    j++;
                }
                for(int i=0; i<tamVetor/2; i++){
                    vetOrdena[j]=vetAux[i];
                    j++;
                }
                BubbleSort( vetOrdena, tamVetor);
                
                // devolvo os valores que recebi para a direita
                j=0;
                for(int i=tamVetor/2; i<tamVetor; i++){
                    vetor[i] = vetOrdena[j];
                    j++;
                }
                for(int i=0; i<tamVetor/2; i++){
                    vetAux[i] = vetOrdena[j];
                    j++;
                }
                MPI_Send(&vetAux, tamVetor/2, MPI_INT, intRank+1, 0, MPI_COMM_WORLD); 

            }
            
            // se não for o 0, recebo de volta os maiores valores da esquerda
            if(intRank > 0){
                printf("Nois 3");
                MPI_Recv(&vetAux, tamVetor/2, MPI_INT, 0, intRank-1, 0, &status);
                for(int i=0; i<tamVetor/2; i++){
                    vetor[i] = vetAux[i];
                }
            }
            //Reseta o vetor para controlar os processos que estão ou não ok
            for(int i = 0; i<proc_n; i++){ 
                vetDesord[i]=0;
            }

        }

    }

    t2 = MPI_Wtime();
    double time=(t2-t1) ;
    printf("\nTempo de execução: %f\n\n", time);
    
    MPI_Finalize();

    return 1;
 }



 /*
 ALGORITMO

Versão paralela seguindo o modelo fases paralelas, de um programa que ordena um grande vetor usando o algortimo Bubble Sort. 
Cada um dos processos é responsável por 1/np do vetor (neste caso 1/16), que já pode ser gerado internamente em cada processo,
     sem a necessidade de gerar em um único processo e depois distribuir entre os restantes. 
Depois, o processamento segue as seguintes fases, que são executadas em paralelo por todos os processos até a conclusão da ordenação (por isso o nome do modelo):

Ordenação local: cada processo ordena a sua parte do vetor global
É feita uma verificação distribuída para identificar se o vetor global esta ordenado:
    cada processo envia o seu maior valor para o vizinho da direita, este compara com o seu menor valor e verifica se os dois processos estão ordenados entre si. Como todos os processos fazem esta operação
        , cada um sabe se está ordenado em relação ao vizinho da esquerda. Esta informação é distribuída para todos os processos com uma operação de comunicação coletiva (Broadcast).
    Se todos estiverem ordenados entre si, todos terminam
Se não, cada processo troca uma parte dos seus valores mais baixos com os mais altos do vizinho da esquerda.
    A ideia é empurrar os valores que estão fora de lugar para os processos corretos, e volta para a fase 1.
------------------------------------------------------------------------------------------------------------------------------

0: Gera vetor global em ordem decrescente
1: Divide o vetor em trechos iguais para cada processo
Enquanto não encerrar:
    2: Cada processo aplica bubble sort no seu trecho do vetor.
    3: Verifica se todos os processos estão ordenados entre si, verificando se o seu número maior no trecho é menor que o menor do próximo trecho.
        3.5: Processos que não estiverem ordenados com o vizinho avisam o controle.
    4: Cada processo passa a primeira metade do seu trecho ordenado para o processo anterior e este devolve sua metade mais alta.*/
